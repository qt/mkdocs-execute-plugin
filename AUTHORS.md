# mkdocs-execute-plugin Authors

Below is a list of the contributors to Python-MUMPS

+ Hugo Kerstens
+ [Anton Akhmerov](<https://antonakhmerov.org>)
+ [Juan Daniel Torres](<https://jdtorres.org>)


For a full list of contributors run

```bash
git log --pretty=format:"%an" | sort | uniq
```
